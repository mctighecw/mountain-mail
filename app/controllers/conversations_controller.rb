class ConversationsController < ApplicationController
  before_action :authenticate_user!

  def index
    redirect_to root_path
  end

  def new
  end

  def create
    recipients = User.where(id: conversation_params[:recipients])
    conversation = current_user.send_message(recipients, conversation_params[:body], conversation_params[:subject]).conversation
    flash[:success] = "Your message has been sent successfully."
    redirect_to conversation_path(conversation)
  end

  def show
    @receipts = conversation.receipts_for(current_user).order("created_at ASC")
    conversation.mark_as_read(current_user)
  end

  def reply
    current_user.reply_to_conversation(conversation, message_params[:body])
    flash[:success] = 'Reply sent.'
    redirect_to conversation_path(conversation)
  end

  def trash
    conversation.move_to_trash(current_user)
    flash[:success] = 'The conversation has been moved to the trash.'
    redirect_to mailbox_inbox_path
  end

  def untrash
    conversation.untrash(current_user)
    flash[:success] = 'The conversation has been restored.'
    redirect_to mailbox_inbox_path
  end

  def empty_trash
    current_user.mark_as_deleted(mailbox.trash.to_a)
    redirect_to mailbox_trash_path
  end

  private
  def conversation_params
    params.require(:conversation).permit(:subject, :body, recipients:[])
  end

  def message_params
    params.require(:message).permit(:body, :subject)
  end

end
