class UsersController < ApplicationController
  def index
    if user_signed_in?
      @users = User.order('created_at DESC').paginate(page: params[:page], per_page: 20)
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  def my_profile
    @user = current_user
  end

end