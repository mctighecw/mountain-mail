// Load Turbolinks
$(document).on("turbolinks:load", function(){

  // Enable chosen jQuery plugin
    $('.chosen-select').chosen({
        placeholder_text_multiple: 'Choose recipients',
        no_results_text: 'No results matched'
    });

  // Fade out alerts
  $(".alert, .notice").delay(2000).fadeOut(800);

});