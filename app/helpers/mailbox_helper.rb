module MailboxHelper
  def unread_messages_count
    mailbox.inbox(unread: true).count
  end

  def unread_messages
    mailbox.inbox(unread: true).present?
  end

end