# README

This is a Rails web app that allows users to send and receive messages.

## App Information

App Name: mountain-mail

Created: August 2016

Updated: August 2020; September 2022; January 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/mountain-mail)

Production: [Link](https://mountain-mail.mctighecw.site)

## Running App

### Locally

Get app info:

```sh
$ rails about
```

Update gems, if necessary:

```sh
$ bundle update
```

Install gems:

```sh
$ bundle install
```

Start dev server:

```sh
$ bin/rails server
```

### With Docker

1. Fill out `.env` file values

2. Build Docker image and start container

```sh
$ docker-compose up -d --build
```

## Notes

- Ruby version 2.6.5
- Rails version 6.0.3
- This app features the Mailboxer gem for messaging functionality
- Originally deployed on [Heroku](https://www.heroku.com)

Last updated: 2025-02-24
