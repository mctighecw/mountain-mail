Rails.application.routes.draw do
  devise_for :users
  resources :users, only: [:index, :my_profile]

  resources :conversations do
    member do
      post :reply
      post :trash
      post :untrash
    end
    collection do
      delete :empty_trash
    end
  end

  resources :messages, only: [:new, :create]

  get 'mailbox/inbox', to: 'mailbox#inbox', as: :mailbox_inbox
  get 'mailbox/sent', to: 'mailbox#sent', as: :mailbox_sent
  get 'mailbox/trash', to: 'mailbox#trash', as: :mailbox_trash

  # Root
  root 'pages#home'

  devise_scope :user do
    get '/sign_up', to: 'devise/registrations#new'
    get '/log_in', to: 'devise/sessions#new'
  end

  get '/my_profile', to: 'users#my_profile'

end
